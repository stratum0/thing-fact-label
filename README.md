Thing Fact Labels
=================

Descriptive Labels for Things.  Print it on adhesive paper, fill out the
blanks, stick it onto your things.

## Dependencies

- [Yanone Kaffeesatz with ZeroHack][zerohack].
	* Optionally, get the original [Yanone Kaffeesatz][kaffeesatz] (or [FF
	Kava][ffkava], which is based on Yanone Kaffeesatz), edit the SVGs in a text
	editor and replace every occurrence of `Yanone Kaffeesatz ZeroHack` with the
	new font name.

[zerohack]: https://stratum0.org/wiki/Yanone_Kaffeesatz_ZeroHack
[kaffeesatz]: https://www.yanone.de/fonts/kaffeesatz
[ffkava]: https://www.yanone.de/typedesign/kava/
