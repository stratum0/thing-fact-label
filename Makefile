all: pdf

thinglabel-203dpi.png:
	inkscape -z -C "Thing Fact Label 2.25x4.00 inch.svg" -d 203 -e $@

pdf:
	inkscape -z -C "Thing Fact Label 2.25x4.00 inch.svg" -A "Thing Fact Label 2.25x4.00 inch.pdf"
	inkscape -z -C "Thing Fact Label 2.25x4.00 inch small x7 for cutting.svg" -A "Thing Fact Label 2.25x4.00 inch small x7 for cutting.pdf"
	inkscape -z -C "Thing Fact Label 2.25x4.00 inch landscape.svg" -A "Thing Fact Label 2.25x4.00 inch landscape.pdf"
	pdf90 "Thing Fact Label 2.25x4.00 inch landscape.pdf"
	inkscape -z -C "Thing Fact Label 2.25x1.25 inch.svg" -A "Thing Fact Label 2.25x1.25 inch.pdf"
